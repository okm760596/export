
const gnbBtn = document.querySelector(".tit_gnb .btn_menu");
const depth1BtnAll = document.querySelectorAll(".gnb-1depth_item");
const depth2BtnAll = document.querySelectorAll(".gnb-2depth_item");
$(document).ready(function () {
    $(".open-modal").on("click", uiModal.open); //modal popup
    $(".open-modal-to").on("click", uiModal.openModalTo); //modal to modal popup
    $(document).mouseup(function (e) {
        uiModal.clickModalOut(e);
    });


    // GNB Menu button click event
    gnbBtn.addEventListener('click', uiNavi.open)
    // GNB Menu Depths click events
    depth1BtnAll.forEach(function(depth1Btn){
        depth1Btn.removeEventListener('click', uiNavi.depth1);
        depth1Btn.addEventListener('click', uiNavi.depth1);
    });
    depth2BtnAll.forEach(function(depth2Btn){
        depth2Btn.removeEventListener('click', uiNavi.depth2);
        depth2Btn.addEventListener('click', uiNavi.depth2);
    });
    //
    btnPageScrollTop();
    textInputCheck();
    mainUiTab();
    mainUiQuick();
    uiBtnSearch();
    uiModal.floatingMenuCheck();
});


function textInputCheck(){
    $(".input-box_wrap input").on("change keyup paste", function(){
        if ($(this).val().length > 0){
            //$(this).parents('label').addClass('on');
        } else {
            //$(this).parents('label').removeClass('on');
        }
        
    })
}

function uiBtnSearch(){
    $('.btn_search').on("click",function(){
        $('body').toggleClass('sch_on');
        if($('body').hasClass('sch_on')){
            setTimeout(function(){
                $('.sch_box input:text').focus();
            }, 100)
        }
    });
}

function mainUiQuick(){
    $('.btn_floating_open').click(function() {
        $(this).parents('.m-floating_wrap').addClass('on');
    });

    $('.m-floating_item.item4').click(function() {
        $(this).parents('.m-floating_wrap').removeClass('on');
    });
}

function mainUiTab(){
    $('.btn_tab').click(function() {
        var tab = $(this).attr('data-tab');
        $("#"+tab).addClass('active').siblings().removeClass('active')
        $(this).addClass('active').siblings().removeClass('active');
    });
}

function btnPageScrollTop(){
    var $body = $(document.body);
    var speed = 400;
    var $top = '';
    $top = $('<button>').addClass('btn_page_top').html("위로가기").appendTo($body);
    $(window).scroll(function(){
        var y = $(this).scrollTop();
        if (y >= 100){
            $(".btn_page_top").addClass("on");
        } else {
            $(".btn_page_top").removeClass("on");
        }
    });
    $(".btn_page_top").click(function(){
        $('body, html').animate({scrollTop:0}, speed);
    });
}

var mainSwiper = new Swiper('#mainBanner', {
    loop: true,
    autoplay: {
        delay: 3000,
    },
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    slidesPerView: 'auto',
    spaceBetween: 10,
    centeredSlides: true, 
});
var solutionSwiperList = new Swiper('.m-solution_list', {
    slidesPerView: 2.5,
    spaceBetween: 15,
    observer: true,
	observeParents: true,
});
var profileSwiperList = new Swiper('#profileList1', {
    loop: true,
    centeredSlides: true,
    spaceBetween: 5,
    slidesPerView:2.6,
    navigation: {
        nextEl: ".bt_profile_list.swiper-button-next",
        prevEl: ".bt_profile_list.swiper-button-prev",
    },
});
var profileSwiperList = new Swiper('#profileList2', {
    slidesPerView: 3.2,
    spaceBetween: 15,
    
});
var mainSwiper = new Swiper('#bottomBrn', {
    loop: true,
    autoplay: false,
    slidesPerView: 'auto',
    spaceBetween: 10,
    centeredSlides: true, 
    pagination: {
		el: ".pagination_number",
		type: "fraction",
	},
});


var uiNavi = {
    open : function(e){
        //const $target = e.currentTarget;
        if(!document.body.classList.contains('menu_on')) {
            document.body.classList.add('menu_on');
        } else {
            document.body.classList.remove('menu_on');
        }
    },
    depth1 : function(e){
        const $target = e.currentTarget;
        const index = $target.dataset['index'];
        const depth2All = document.querySelectorAll(".sub_depths > li");
        const targetDepth2 = document.querySelector('.sub_depths > li:nth-child('+index+')');
        
        depth1BtnAll.forEach(function(e){
            e.classList.remove('active');
        });
        depth2All.forEach(function(e){
            e.classList.remove('active');
        });
        $target.classList.add('active');
        targetDepth2.classList.add('active');
        // $target.scrollIntoView({
        //     block: "start", inline: "nearest"
        // });
    },
    depth2 : function(e){
        const $target = e.currentTarget;
        if(!$target.classList.contains('active')) {
            $target.classList.add('active');
        } else {
            $target.classList.remove('active');
        }
        // $target.scrollIntoView({
        //     block: "start", inline: "nearest"
        // });
    }
}


var uiModal = {
    init : function(){
        
    },
    floatingMenuCheck : function(){
        var $target = $(".floating-btn_wrap");
        if($target.length > 0 && $target.hasClass('active')){
            $('html').addClass('floating_on');
        }
    },
    open : function(){
        var target = $(this);
        var modalId = target.data("modal");
        $("#" + modalId)
            .addClass("active")
            .attr("tabindex", 0)
            .focus();
        if ($("#" + modalId).find(".modal-title").length > 0) {
            $("#" + modalId).addClass("scrollH1");
        }
        $("body").addClass("scrollHide");
        $(".modal_close").on("click", {
            btn : target, 
            id : modalId
        }, uiModal.close);
      
        // if (target.data('anchor').length > 0) {
        //   alert(target.data('anchor'));
        // }
        uiModal.modalSetInit(modalId);
        return false;
    },
    openModalTo : function(){
        var modalId = $(this).data("modal");
        var target = $(this);

        $("#" + modalId)
            .addClass("active")
            //.css("background", "none")
            .attr("tabindex", 0);
        // .focus();

        $("#" + modalId).find(".modal_close").on("click", function () {
            $(this).parents(".modal").addClass('delay');
            setTimeout(function() {
                $(this).parents(".modal").removeClass("active").removeClass("delay").closest("body").addClass("scrollHide");
            }, 380);
            
            target.focus();
            $("body").removeClass("scrollHide");
        });

        uiModal.modalSetInit(modalId);
        // 221229 컨텐츠 앵커 처리
        if (target.data('anchor').length > 0) {
          document.getElementById('modalInner1').scrollIntoView();
        }
        return false;
    },
    close : function(e){
        var clickBtn = $(`[data-modal=${e.data.id}]`);
        var target = $(this);
        $(this).parents(".modal").addClass('delay');
        setTimeout(function() { 
            target.parents(".modal").removeClass("active").removeClass("scrollH1").removeClass("delay").attr("tabindex", 0);
        }, 380);
        
        e.data.btn.focus();
        if($(this).parents('.modal').hasClass('readyClose')){
            $(this).parents('.modal').removeClass('readyClose');
        } else {
            $("body").removeClass("scrollHide");
        }
        //removeDisable(clickBtn);
    },
    clickModalOut : function(e){
        var findClass = e.target.className;
        var $thisId = $('#'+e.target.id);
        var modal = $(".modal");

        if (findClass.match("modal") && findClass.match("active")) {
            $thisId.addClass('delay');
            setTimeout(function() { 
                $thisId.removeClass("active").removeClass('delay');
            }, 380);
            
            if($thisId.hasClass('readyClose')){
                $thisId.removeClass('readyClose');
            } else {
                $("body").removeClass("scrollHide");  
            }
        }
        
        if (e.target.id == "") return;
        var cilickBtn = $(`[data-modal=${e.target.id}]`);
        //console.log(cilickBtn);
    },
    modalSetInit : function(e){
        var $target = "#" + e;  
        // 팝업 scroll 초기화
        if($($target).find('.modal-con').length > 0) { 
            $($target).find('.modal-con').scrollTop(0);
        }
    }
    
}

